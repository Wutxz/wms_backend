const express = require("express");
const app = express();
const helmet = require("helmet");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const fileUpload = require("express-fileupload");
const passport = require("passport");
const config = require("./app/configs/index");
const path = require("path");
const port = process.env.PORT || 3000;

//import middleware
const errorHandler = require("./app/middlewares/ErrorHandler");

//router
const indexRouter = require("./app/routes/index");

app.set("trust proxy", 1);

// use libary
app.use(config.LIMITER); // apply to all requests
app.use(helmet());
app.use(cookieParser());
app.use(bodyParser.json(config.BODY_JSON));
app.use(bodyParser.urlencoded(config.BODY_URLENCODED));
app.use(cors());
app.use(fileUpload());
app.use(express.static(path.join(__dirname, "./public")));

//init passport
app.use(passport.initialize());

//init router
app.use(indexRouter);
// init Error
app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
