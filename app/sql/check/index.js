const CheckDb = {
  addCheck(matId, num, date, userId, id, matNum) {
    let queryString = `DECLARE @num INT
    SELECT @num = Check_Num FROM [Check] WHERE Check_Id = 'CH${id}' AND Mat_Id = '${matId}'
    IF EXISTS (SELECT Check_Id FROM [Check] WHERE Check_Id = 'CH${id}' AND Mat_Id = '${matId}')
       BEGIN
            UPDATE [Check] SET Check_Num = @num + ${num}
            WHERE Check_Id = 'CH${id}' AND Mat_Id = '${matId}'
       END
    ELSE
       BEGIN
            INSERT INTO [Check] (Check_Id, Check_Date, Check_Num, Check_Status, Mat_Value, User_Id, Mat_Id)
            VALUES ('CH${id}', '${date}', ${num}, 1, ${matNum}, '${userId}', '${matId}')
       END`;
    return queryString;
  },
  addReturn(matId, num, date, userId, id, matNum) {
    let queryString = `DECLARE @num INT
    SELECT @num = Return_Num FROM [Return] WHERE Return_Id = 'RE${id}' AND Mat_Id = '${matId}'
    IF EXISTS (SELECT Return_Id FROM [Return] WHERE Return_Id = 'RE${id}' AND Mat_Id = '${matId}')
       BEGIN
            UPDATE [Return] SET Return_Num = @num + ${num}
            WHERE Return_Id = 'RE${id}' AND Mat_Id = '${matId}'
       END
    ELSE
       BEGIN
            INSERT INTO [Return] (Return_Id, Return_Date, Return_Num, Return_Status, Mat_Value, User_Id, Mat_Id)
            VALUES ('RE${id}', '${date}', ${num}, 1, ${matNum}, '${userId}', '${matId}')
       END`;
    return queryString;
  },
  getCheckInfoByMat(matId) {
    let queryString = `SELECT TOP 1 Check_Date, Check_Num FROM [Check]
    WHERE Mat_Id = '${matId}'
    ORDER BY Check_Date DESC`;
    return queryString;
  },
  getRecentCheckIdByComp(str) {
    let queryString = `SELECT TOP 1 Check_Id, Check_Date FROM [Check]
    WHERE Check_Id LIKE '__${str}____'
    ORDER BY Check_Id DESC`;
    return queryString;
  },
  getRecentReturnIdByComp(str) {
    let queryString = `SELECT TOP 1 Return_Id, Return_Date FROM [Return]
    WHERE Return_Id LIKE '__${str}____'
    ORDER BY Return_Id DESC`;
    return queryString;
  },
};

module.exports = CheckDb;
