const NotificationDb = {
  addNotiAd(name, date, adminId, typeId) {
    let queryString = `INSERT INTO Notification (Noti_Id, Noti_Name, Noti_Date, Noti_Status, Noti_Type_Id, Admin_Id)
      SELECT 
        'NF' + RIGHT('0000' + CAST(Noti_Id + 1 AS VARCHAR(4)), 4),
        '${name}',
        '${date}',
        0,
        '${typeId}',
        '${adminId}'
      FROM (
        SELECT TOP 1 Noti_Id = CAST(RIGHT(Noti_Id, 4) AS INT)
        FROM Notification
        ORDER BY Noti_Id DESC
      ) t`;
    return queryString;
  },
  getNotiAd(adminId) {
    let queryString = `SELECT TOP 5 t1.Noti_Id, t1.Noti_Name, t1.Noti_Date, t1.Noti_Status, t2.Noti_Type_Name FROM Notification t1
      INNER JOIN NotificationType t2 ON t1.Noti_Type_Id = t2.Noti_Type_Id
      WHERE Admin_Id = '${adminId}' AND t1.Noti_Status BETWEEN 0 AND 1
      ORDER BY t1.Noti_Date DESC, t1.Noti_Status ASC`;
    return queryString;
  },
  addNotiSa(name, date, typeId) {
    let queryString = `INSERT INTO Notification (Noti_Id, Noti_Name, Noti_Date, Noti_Status, Noti_Type_Id)
      SELECT 
        'NF' + RIGHT('0000' + CAST(Noti_Id + 1 AS VARCHAR(4)), 4),
        '${name}',
        '${date}',
        0,
        '${typeId}'
      FROM (
        SELECT TOP 1 Noti_Id = CAST(RIGHT(Noti_Id, 4) AS INT)
        FROM Notification
        ORDER BY Noti_Id DESC
      ) t`;
    return queryString;
  },
  getNotiSa() {
    let queryString = `SELECT TOP 5 t1.Noti_Id, t1.Noti_Name, t1.Noti_Date, t1.Noti_Status, t2.Noti_Type_Id, t2.Noti_Type_Name FROM Notification t1
      INNER JOIN NotificationType t2 ON t1.Noti_Type_Id = t2.Noti_Type_Id
      WHERE Admin_Id IS NULL AND t1.Noti_Status BETWEEN 0 AND 1
      ORDER BY t1.Noti_Date DESC, t1.Noti_Status ASC`;
    return queryString;
  },
  updateStatus(notiId) {
    let queryString = `UPDATE Notification
    SET Noti_Status = 1
    WHERE Noti_Id = '${notiId}'`;
    return queryString;
  },
};

module.exports = NotificationDb;
