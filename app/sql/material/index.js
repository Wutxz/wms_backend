const MaterialDb = {
  getCateList(compId) {
    let queryString = `SELECT Mat_Cate_Id, Mat_Cate_Name, Mat_Cate_Desc FROM MaterialCate
    WHERE Comp_Id = '${compId}'`;
    return queryString;
  },
  getTypeList(cateId) {
    let queryString = `SELECT Mat_Type_Id, Mat_Type_Name, Mat_Type_Desc FROM MaterialType
    WHERE Mat_Cate_Id = '${cateId}'`;
    return queryString;
  },
  getTypeListByComp(compId) {
    let queryString = `SELECT Mat_Type_Id, Mat_Type_Name FROM MaterialType t1
    INNER JOIN MaterialCate t2 ON t1.Mat_Cate_Id = t2.Mat_Cate_Id
    WHERE t2.Comp_Id = '${compId}'`;
    return queryString;
  },
  getListByType(typeId) {
    let queryString = `SELECT Mat_Id, Mat_Name, Mat_Color, Mat_Size, Mat_Num, Mat_Min FROM Material
    WHERE Mat_Type_Id = '${typeId}'`;
    return queryString;
  },
  addCate(name, desc, id, compId) {
    let queryString = `INSERT INTO MaterialCate (Mat_Cate_Id, Mat_Cate_Name, Mat_Cate_Desc, Comp_Id)
    VALUES ('MC${id}', '${name}', '${desc}', '${compId}')`;
    return queryString;
  },
  addType(name, desc, cateId, id, compId) {
    let queryString = `INSERT INTO MaterialType (Mat_Type_Id, Mat_Type_Name, Mat_Type_Desc, Mat_Cate_Id, Comp_Id)
    VALUES ('MT${id}', '${name}', '${desc}', '${cateId}', '${compId}')`;
    return queryString;
  },
  edit(name, color, size, min, matId) {
    let queryString = `UPDATE Material
    SET Mat_Name = '${name}', Mat_Color = '${color}', Mat_Size = ${size}, Mat_Min = '${min}'
    WHERE Mat_Id = '${matId}'`;
    return queryString;
  },
  editCate(name, desc, cateId) {
    let queryString = `UPDATE MaterialCate
    SET Mat_Cate_Name = '${name}', Mat_Cate_Desc = '${desc}'
    WHERE Mat_Cate_Id = '${cateId}'`;
    return queryString;
  },
  editType(name, desc, typeId) {
    let queryString = `UPDATE MaterialType
    SET Mat_Type_Name = '${name}', Mat_Type_Desc = '${desc}'
    WHERE Mat_Type_Id = '${typeId}'`;
    return queryString;
  },
  delete(matId) {
    let queryString = `DELETE FROM Material
    WHERE Mat_Id = '${matId}'`;
    return queryString;
  },
  deleteCate(cateId) {
    let queryString = `DELETE FROM MaterialCate
    WHERE Mat_Cate_Id = '${cateId}'`;
    return queryString;
  },
  deleteType(typeId) {
    let queryString = `DELETE FROM MaterialType
    WHERE Mat_Type_Id = '${typeId}'`;
    return queryString;
  },
  getInfoById(matId) {
    let queryString = `SELECT Mat_Name, Mat_Num, Mat_Price, Unit_Id, Store_Id FROM Material
    WHERE Mat_Id = '${matId}'`;
    return queryString;
  },
  updateNum(matId, num) {
    let queryString = `UPDATE Material
    SET Mat_Num = ${num}
    WHERE Mat_Id = '${matId}'`;
    return queryString;
  },
  getRecentMatNum(name) {
    let queryString = `SELECT Mat_Num FROM Material
    WHERE Mat_Id = '${name}'`;
    return queryString;
  },
  getRecentMatIdByComp(str) {
    let queryString = `SELECT TOP 1 Mat_Id FROM Material
    WHERE Mat_Id LIKE '__${str}____'
    ORDER BY Mat_Id DESC`;
    return queryString;
  },
  getRecentMatCateIdByComp(str) {
    let queryString = `SELECT TOP 1 Mat_Cate_Id FROM MaterialCate
    WHERE Mat_Cate_Id LIKE '__${str}____'
    ORDER BY Mat_Cate_Id DESC`;
    return queryString;
  },
  getRecentMatTypeIdByComp(str) {
    let queryString = `SELECT TOP 1 Mat_Type_Id FROM MaterialType
    WHERE Mat_Type_Id LIKE '__${str}____'
    ORDER BY Mat_Type_Id DESC`;
    return queryString;
  },
  //RECEIVE
  getRecentRecIdByComp(str) {
    let queryString = `SELECT TOP 1 Receive_Id, Receive_Date FROM [Receive]
    WHERE Receive_Id LIKE '__${str}____'
    ORDER BY Receive_Id DESC`;
    return queryString;
  },
  addReceive(name, num, date, userId, id) {
    let queryString = `DECLARE @num INT
    SELECT @num = Receive_Num FROM [Receive] WHERE Receive_Id = 'RE${id}' AND Mat_Id = '${name}'
    IF EXISTS (SELECT Receive_Id FROM [Receive] WHERE Receive_Id = 'RE${id}' AND Mat_Id = '${name}')
       BEGIN
            UPDATE [Receive] SET Receive_Num = @num + ${num}
            WHERE Receive_Id = 'RE${id}' AND Mat_Id = '${name}'
       END
    ELSE
       BEGIN
            INSERT INTO [Receive] (Receive_Id, Receive_Date, Receive_Num, User_Id, Mat_Id)
            VALUES ('RE${id}', '${date}', ${num}, '${userId}', '${name}')
       END`;
    return queryString;
  },
  //RETURN
  addReturn(name, num, date, userId) {
    let queryString = `INSERT INTO [Return] (Return_Id, Return_Date, Return_Num, User_Id, Mat_Id)
    SELECT 
      'RT' + RIGHT('0000' + CAST(Return_Id + 1 AS VARCHAR(4)), 4),
      '${date}',
      '${num}',
      '${userId}',
      '${name}'
    FROM (
      SELECT TOP 1 Return_Id = CAST(RIGHT(Return_Id, 4) AS INT)
      FROM [Return]
      ORDER BY Return_Id DESC
    ) t`;
    return queryString;
  },
};

module.exports = MaterialDb;
