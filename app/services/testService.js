const config = require('../configs');
const AdminDb = require('../sql/Admin');
const MaterialDb = require('../sql/material');
const MailService = require('./mailService');
const poolConnect = config.SQLCONNECTION.connect();
const moment = require('moment');
config.SQLCONNECTION.on('error', (err) => {
  console.log(err);
});

exports.WareList = (adminId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(AdminDb.getListWarehouse(adminId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.StoreList = (ware, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(AdminDb.getListStorage(ware));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.OldMatList = (adminId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(AdminDb.getListOldMaterial(adminId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.ReadMatQr = (name, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(AdminDb.getQrMaterial(name));
      let qrId = data.recordset[0].Qr_Id;
      let data2 = await sqlRequest.query(AdminDb.getQrName(qrId));
      let qrName = data2.recordset[0].Qr_Name;
      if (data.recordset.length && data2.recordset.length) {
        resolve(qrName);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.NewMaterial = (
  name,
  sup,
  store,
  num,
  price,
  date,
  unit,
  compId,
  next
) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      // 'MA 000 0001'
      let prefix = '000'; // 3 digits for company
      let prefix2 = '0000'; // 4 digits for auto increment number
      // EX: compId = 'CO 0001'
      // Cut string -> '001' (want 3 digits)
      let x = prefix + parseInt(compId.substring(2, 6)); // '000' + 1 = '0001'
      let str = x.substring(x.length - prefix.length); // x.length = 4; prefix.length = 3; -> '001'
      // Find Mat Id LIKE '__${id}____'
      let dataId = await sqlRequest.query(MaterialDb.getRecentMatIdByComp(str));
      let recMat = dataId.recordset.length ? dataId.recordset[0].Mat_Id : '1'; // 'MA 123 0456' || '1'
      let y = dataId.recordset.length
        ? prefix2 + (parseInt(recMat.substring(5)) + 1)
        : prefix2 + parseInt(recMat); // '0000457' || '00001'
      let str2 = y.substring(y.length - prefix2.length); // '0457' || '0001'
      let id = str + str2; // '0010457' || '0010001'
      let data = await sqlRequest.query(
        AdminDb.addNewMaterial(name, store, num, price, unit, id)
      );
      let data2 = await sqlRequest.query(AdminDb.addNewSupplier(sup));
      let dataMat = await sqlRequest.query(AdminDb.getRecentMaterialId());
      let matId = dataMat.recordset[0].Mat_Id;
      let dataSup = await sqlRequest.query(AdminDb.getRecentSupplierId());
      let supId = dataSup.recordset[0].Sup_Id;
      let data3 = await sqlRequest.query(
        AdminDb.addNewProduction(matId, supId)
      );
      if (
        data.rowsAffected[0] != 0 &&
        data2.rowsAffected[0] != 0 &&
        data3.rowsAffected[0] != 0
      ) {
        resolve(matId);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.NewStorage = (qr, mat, num, date, userId, compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let prefix = '000';
      let prefix2 = '0000';
      let today = moment().format('YYYY-MM-DD');
      let x = prefix + parseInt(compId.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      // rec = data.length ? recent id : '1'
      // y = !data.length ? prefix2 + Int(rec) : recent date === today ? prefix2 + Int(rec.Str(5)) : prefix2 + (Int(rec.Str(5)) + 1)
      let dataId = await sqlRequest.query(MaterialDb.getRecentRecIdByComp(str));
      let rec = dataId.recordset.length ? dataId.recordset[0].Receive_Id : '1';
      let y = !dataId.recordset.length
        ? prefix2 + parseInt(rec)
        : moment(dataId.recordset[0].Receive_Date).format('YYYY-MM-DD') ===
          today
        ? prefix2 + parseInt(rec.substring(5))
        : prefix2 + (parseInt(rec.substring(5)) + 1);
      let str2 = y.substring(y.length - prefix2.length);
      let id = str + str2;
      let data = await sqlRequest.query(AdminDb.addNewQr(qr));
      let dataQr = await sqlRequest.query(AdminDb.getRecentQrId());
      let qrId = dataQr.recordset[0].Qr_Id;
      let data2 = await sqlRequest.query(AdminDb.addNewMaterial2(mat, qrId));
      let data3 = await sqlRequest.query(
        MaterialDb.addReceive(mat, num, date, userId, id)
      );
      if (
        data.rowsAffected[0] != 0 &&
        data2.rowsAffected[0] != 0 &&
        data3.rowsAffected[0] != 0
      ) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.Login = (username, password, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let spAdmin = await sqlRequest.query(
        AdminDb.checkSpAdmin(username, password)
      );
      let admin = await sqlRequest.query(
        AdminDb.checkAdmin(username, password)
      );
      let user = await sqlRequest.query(AdminDb.checkUser(username, password));
      if (spAdmin.recordset.length) {
        resolve(spAdmin.recordset);
      } else if (admin.recordset.length) {
        resolve(admin.recordset);
      } else if (user.recordset.length) {
        resolve(user.recordset);
      } else {
        const error = new Error('ชื่อผู้ใช้ หรือรหัสผ่านไม่ถูกต้อง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.Login2 = (username, password, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        AdminDb.checkSpAdmin(username, password)
      );
      let data2 = await sqlRequest.query(
        AdminDb.checkAdmin(username, password)
      );
      let data3 = await sqlRequest.query(AdminDb.checkUser(username, password));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else if (data2.recordset.length) {
        resolve(data2.recordset);
      } else if (data3.recordset.length) {
        resolve(data3.recordset);
      } else {
        const error = new Error('ชื่อผู้ใช้ หรือรหัสผ่านไม่ถูกต้อง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.Register = (
  username,
  password,
  email,
  company,
  name,
  surname,
  companyAddress,
  tel,
  headNo,
  next
) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let valid = await sqlRequest.query(
        AdminDb.CheckAdminByNo(headNo, username)
      );
      let valid2 = await sqlRequest.query(AdminDb.CheckUserByNo(username));
      if (!valid.recordset.length && !valid2.recordset.length) {
        let admin = await sqlRequest.query(
          AdminDb.registerAdmin(company, companyAddress)
        );
        let dataComp = await sqlRequest.query(AdminDb.getRecentCompanyId());
        let compId = dataComp.recordset[0].Comp_Id;
        let prefix = '000';
        let prefix2 = '0000';
        let date = moment().format('YYYY-MM-DD');
        let x = prefix + parseInt(compId.substring(2, 6));
        let str = x.substring(x.length - prefix.length);
        let dataId = await sqlRequest.query(AdminDb.getRecentAdminByComp(str));
        let rec = dataId.recordset.length ? dataId.recordset[0].Admin_Id : '1';
        let y = dataId.recordset.length
          ? prefix2 + (parseInt(rec.substring(5)) + 1)
          : prefix2 + parseInt(rec);
        let str2 = y.substring(y.length - prefix2.length);
        let id = str + str2;
        let admin2 = await sqlRequest.query(
          AdminDb.registerAdmin2(
            id,
            username,
            password,
            email,
            name,
            surname,
            tel,
            headNo,
            compId
          )
        );
        let dataAd = await sqlRequest.query(AdminDb.getRecentAdminId());
        let adminId = dataAd.recordset[0].Admin_Id;
        let admin3 = await sqlRequest.query(
          AdminDb.registerAdmin3(adminId, date)
        );
        if (
          admin.rowsAffected[0] != 0 &&
          admin2.rowsAffected[0] != 0 &&
          admin3.rowsAffected[0] != 0
        ) {
          resolve('OK');
        } else {
          let error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
          error.statusCode = 400;
          throw error;
        }
      } else {
        let error = new Error(
          'ชื่อผู้ใช้หรือเลขประจำตัวนี้ถูกลงทะเบียนแล้ว กรุณาลองใหม่อีกครั้ง'
        );
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.Admin = (compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(AdminDb.getAdmin(compId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.User = (compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(AdminDb.getUser(compId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.Company = (next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(AdminDb.getCompany());
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.WaitCompany = (next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(AdminDb.getWaitCompany());
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.EditCompany = (company, companyAddress, companyId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        AdminDb.editCompany(company, companyAddress, companyId)
      );
      if (data.rowsAffected[0] != 0) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.EditCompanyStatus = (maxAdmin, maxUser, status, companyId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        AdminDb.editCompanyStatus(status, companyId)
      );
      let data2 = await sqlRequest.query(
        AdminDb.editCompanyMax(maxAdmin, maxUser, companyId)
      );
      if (data.rowsAffected[0] != 0 && data2.rowsAffected[0] != 0) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.EditUser = (name, surname, tel, userId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        AdminDb.editUser(name, surname, tel, userId)
      );
      if (data.rowsAffected[0] != 0) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.EditAdmin = (name, surname, tel, adminId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        AdminDb.editAdmin(name, surname, tel, adminId)
      );
      if (data.rowsAffected[0] != 0) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.EditAdminStatus = (status, adminId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        AdminDb.editAdminStatus(status, adminId)
      );
      if (data.rowsAffected[0] != 0) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.EditUserStatus = (status, userId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(AdminDb.editUserStatus(status, userId));
      if (data.rowsAffected[0] != 0) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.RejectCompany = (status, companyId, email, note, name, adminId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let con = 'ปฏิเสธคำขอเข้าร่วมจากระบบ';
      let data = await sqlRequest.query(
        AdminDb.editCompanyStatus(status, companyId)
      );
      let data2 = await sqlRequest.query(
        AdminDb.editAdminStatus(status, adminId)
      );
      let sendMail = await MailService.Reject(email, note, name, con);
      if (data.rowsAffected[0] != 0 && data2.rowsAffected[0] != 0 && sendMail == 'OK') {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.RemoveCompany = (status, companyId, email, note, arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let con = 'ถอนออกจากระบบ';
      let sendMail = [];
      for (let i = 0; i < arr.length; i++) {
        await sqlRequest.query(
          AdminDb.editCompanyStatus(status, arr[i].Comp_Id)
        );
      }
      if (arr.length === 1) {
        sendMail = await MailService.Remove(email, note, arr[0].Comp_Name, con);
      }
      if (arr.length || sendMail === 'OK') {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.RemoveAdmin = (status, arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let i = 0; i < arr.length; i++) {
        await sqlRequest.query(
          AdminDb.editAdminStatus(status, arr[i].Admin_Id)
        );
      }
      resolve('OK');
    } catch (error) {
      next(error);
    }
  });
};

exports.RemoveUser = (status, arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let i = 0; i < arr.length; i++) {
        await sqlRequest.query(AdminDb.editUserStatus(status, arr[i].User_Id));
      }
      if (arr.length) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.CountCompany = (next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(AdminDb.getCountCompany());
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.CountWaitCompany = (next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(AdminDb.getCountWaitCompany());
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.ConfirmCompany = (status, companyId, adminId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        AdminDb.editCompanyStatus(status, companyId)
      );
      let data2 = await sqlRequest.query(
        AdminDb.editAdminStatus(status, adminId)
      );
      if (data.rowsAffected[0] != 0 && data2.rowsAffected[0] != 0) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 401;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};
