const config = require('../configs');
const SupplierDb = require('../sql/supplier');
const poolConnect = config.SQLCONNECTION.connect();
config.SQLCONNECTION.on('error', (err) => {
  console.log(err);
});

exports.InfoListByMat = (matId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        SupplierDb.getInfoByMat(matId)
      );
      if (data.recordset[0]) {
        resolve(data.recordset);
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};
