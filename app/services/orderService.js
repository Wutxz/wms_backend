const config = require('../configs');
const OrderDb = require('../sql/order');
const poolConnect = config.SQLCONNECTION.connect();
config.SQLCONNECTION.on('error', (err) => {
  console.log(err);
});

exports.NewOrder = (arr, date, userId, compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let prefix = '000';
      let prefix2 = '0000';
      let x = prefix + parseInt(compId.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      let dataId = await sqlRequest.query(OrderDb.getRecentOrderIdByComp(str));
      let rec = dataId.recordset.length ? dataId.recordset[0].Order_Id : '1';
      let y = dataId.recordset.length
        ? prefix2 + (parseInt(rec.substring(5)) + 1)
        : prefix2 + parseInt(rec);
      let str2 = y.substring(y.length - prefix2.length);
      let id = str + str2;
      let data = await sqlRequest.query(OrderDb.addOrder(date, userId, id));
      // let dataId = await sqlRequest.query(OrderDb.getRecentOrderId());
      // let orderId = dataId.recordset[0].Order_Id
      for (let i = 0; i < arr.length; i++) {
        await sqlRequest.query(OrderDb.addOrder2(id, arr[i].proId, arr[i].num));
      }
      if (data.rowsAffected[0] != 0 && arr.length) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.ListByUser = (userId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(OrderDb.getOrderByUser(userId));
      if (data.recordset[0]) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.GoodsList = (orderId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(OrderDb.getGoodsList(orderId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.Status = (orderId, status, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(OrderDb.updateStatus(orderId, status));
      if (data.rowsAffected[0] != 0) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.ListByAdmin = (adminId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(OrderDb.getOrderByAdmin(adminId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};
