const config = require('../configs');
const MaterialDb = require('../sql/material');
const poolConnect = config.SQLCONNECTION.connect();
const moment = require('moment');
config.SQLCONNECTION.on('error', (err) => {
  console.log(err);
});

exports.CateList = (compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(MaterialDb.getCateList(compId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.TypeList = (cateId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(MaterialDb.getTypeList(cateId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.TypeListByComp = (compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(MaterialDb.getTypeListByComp(compId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.ListByType = (typeId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(MaterialDb.getListByType(typeId));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.AddCate = (arr, compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let prefix = '000';
      let prefix2 = '0000';
      let n = false;
      let x = prefix + parseInt(compId.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      let dataId = await sqlRequest.query(
        MaterialDb.getRecentMatCateIdByComp(str)
      );
      let rec = dataId.recordset.length ? dataId.recordset[0].Mat_Cate_Id : '1';
      for (let i = 0; i < arr.length; i++) {
        let y = dataId.recordset.length
          ? prefix2 + (parseInt(rec.substring(5)) + (i + 1))
          : n
          ? prefix2 + (parseInt(rec) + i)
          : prefix2 + parseInt(rec);
        let str2 = y.substring(y.length - prefix2.length);
        let id = str + str2;
        await sqlRequest.query(
          MaterialDb.addCate(arr[i].name, arr[i].desc, id, compId)
        );
        n = true;
      }
      if (arr.length) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.AddType = (arr, compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let prefix = '000';
      let prefix2 = '0000';
      let n = false;
      let x = prefix + parseInt(compId.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      let dataId = await sqlRequest.query(
        MaterialDb.getRecentMatTypeIdByComp(str)
      );
      let rec = dataId.recordset.length ? dataId.recordset[0].Mat_Type_Id : '1';
      for (let i = 0; i < arr.length; i++) {
        let y = dataId.recordset.length
          ? prefix2 + (parseInt(rec.substring(5)) + (i + 1))
          : n
          ? prefix2 + (parseInt(rec) + i)
          : prefix2 + parseInt(rec);
        let str2 = y.substring(y.length - prefix2.length);
        let id = str + str2;
        await sqlRequest.query(
          MaterialDb.addType(
            arr[i].name,
            arr[i].desc,
            arr[i].cateId,
            id,
            compId
          )
        );
        n = true;
      }
      if (arr) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.Edit = (name, color, size, min, matId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        MaterialDb.edit(name, color, size, min, matId)
      );
      if (data.rowsAffected) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.EditCate = (name, desc, cateId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        MaterialDb.editCate(name, desc, cateId)
      );
      if (data.rowsAffected) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.EditType = (name, desc, typeId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data = await sqlRequest.query(
        MaterialDb.editType(name, desc, typeId)
      );
      if (data.rowsAffected) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.Delete = (arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let i = 0; i < arr.length; i++) {
        await sqlRequest.query(MaterialDb.delete(arr[i].Mat_Id));
      }
      if (arr.length) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.DeleteCate = (arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let i = 0; i < arr.length; i++) {
        await sqlRequest.query(MaterialDb.deleteCate(arr[i].Mat_Cate_Id));
      }
      if (arr.length) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.DeleteType = (arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let i = 0; i < arr.length; i++) {
        await sqlRequest.query(MaterialDb.deleteType(arr[i].Mat_Type_Id));
      }
      if (arr.length) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.InfoList = (matId, name, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let data =
        typeof matId === 'string'
          ? await sqlRequest.query(MaterialDb.getInfoById(matId))
          : await sqlRequest.query(MaterialDb.getInfoById(name));
      if (data.recordset.length) {
        resolve(data.recordset);
      } else {
        const error = new Error('ไม่พบข้อมูล กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.DeleteMulti = (arr, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      for (let i = 0; i < arr.length; i++) {
        let num = arr[i].matNum - arr[i].num;
        await sqlRequest.query(MaterialDb.updateNum(arr[i].matId, num));
      }
      if (arr.length) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};

exports.UpdateOld = (name, num, date, userId, compId, next) => {
  return new Promise(async (resolve) => {
    try {
      const pool = await poolConnect;
      const sqlRequest = pool.request();
      let prefix = '000';
      let prefix2 = '0000';
      let today = moment().format('YYYY-MM-DD');
      let x = prefix + parseInt(compId.substring(2, 6));
      let str = x.substring(x.length - prefix.length);
      // rec = data.length ? recent id : '1'
      // y = !data.length ? prefix2 + Int(rec) : recent date === today ? prefix2 + Int(rec.Str(5)) : prefix2 + (Int(rec.Str(5)) + 1)
      let dataId = await sqlRequest.query(MaterialDb.getRecentRecIdByComp(str));
      let rec = dataId.recordset.length ? dataId.recordset[0].Receive_Id : '1';
      let y = !dataId.recordset.length
        ? prefix2 + parseInt(rec)
        : moment(dataId.recordset[0].Receive_Date).format('YYYY-MM-DD') ===
          today
        ? prefix2 + parseInt(rec.substring(5))
        : prefix2 + (parseInt(rec.substring(5)) + 1);
      let str2 = y.substring(y.length - prefix2.length);
      let id = str + str2;
      let dataNum = await sqlRequest.query(MaterialDb.getRecentMatNum(name));
      let matNum = dataNum.recordset[0].Mat_Num + num;
      let data = await sqlRequest.query(MaterialDb.updateNum(name, matNum));
      let data2 = await sqlRequest.query(
        MaterialDb.addReceive(name, num, date, userId, id)
      );
      if (data.rowsAffected && data2.rowsAffected) {
        resolve('OK');
      } else {
        const error = new Error('เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
        error.statusCode = 400;
        throw error;
      }
    } catch (error) {
      next(error);
    }
  });
};
