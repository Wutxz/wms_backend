const express = require('express');
const router = express.Router();
const NotificationService = require('../services/notificationService');

router.post('/adds/ad', async (req, res, next) => {
  try {
    const { name, date, adminId, typeId } = req.body;
    const result = await NotificationService.NotiAd(
      name,
      date,
      adminId,
      typeId,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/admins/ad', async (req, res, next) => {
  try {
    const { adminId } = req.body;
    const result = await NotificationService.NotiAdList(adminId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/adds/sa', async (req, res, next) => {
  try {
    const { name, date, typeId } = req.body;
    const result = await NotificationService.NotiSa(name, date, typeId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.get('/admins/sa', async (req, res, next) => {
  try {
    const result = await NotificationService.NotiSaList(next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/updates/status', async (req, res, next) => {
  try {
    const { notiId } = req.body;
    const result = await NotificationService.Status(notiId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
