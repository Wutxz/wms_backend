const express = require('express');
const router = express.Router();
const MaterialService = require('../services/materialService');

router.post('/cates/list', async (req, res, next) => {
  try {
    const { compId } = req.body;
    const result = await MaterialService.CateList(compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/types/list', async (req, res, next) => {
  try {
    const { cateId } = req.body;
    const result = await MaterialService.TypeList(cateId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/types/search', async (req, res, next) => {
  try {
    const { compId } = req.body;
    const result = await MaterialService.TypeListByComp(compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/list', async (req, res, next) => {
  try {
    const { typeId } = req.body;
    const result = await MaterialService.ListByType(typeId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/cates/add', async (req, res, next) => {
  try {
    const { arr, compId } = req.body;
    const result = await MaterialService.AddCate(arr, compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/types/add', async (req, res, next) => {
  try {
    const { arr, compId } = req.body;
    const result = await MaterialService.AddType(arr, compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/edit', async (req, res, next) => {
  try {
    const { name, color, size, min, matId } = req.body;
    const result = await MaterialService.Edit(
      name,
      color,
      size,
      min,
      matId,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/cates/edit', async (req, res, next) => {
  try {
    const { name, desc, cateId } = req.body;
    const result = await MaterialService.EditCate(name, desc, cateId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/types/edit', async (req, res, next) => {
  try {
    const { name, desc, typeId } = req.body;
    const result = await MaterialService.EditType(name, desc, typeId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/del', async (req, res, next) => {
  try {
    const { selected } = req.body;
    const result = await MaterialService.Delete(selected, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/cates/del', async (req, res, next) => {
  try {
    const { arr } = req.body;
    const result = await MaterialService.DeleteCate(arr, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/types/del', async (req, res, next) => {
  try {
    const { selected } = req.body;
    const result = await MaterialService.DeleteType(selected, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/info', async (req, res, next) => {
  try {
    const { matId, name } = req.body;
    const result = await MaterialService.InfoList(matId, name, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/dels/multi', async (req, res, next) => {
  try {
    const { arr } = req.body;
    const result = await MaterialService.DeleteMulti(arr, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

router.post('/updates/old', async (req, res, next) => {
  try {
    const { name, num, date, userId, compId } = req.body;
    const result = await MaterialService.UpdateOld(
      name,
      num,
      date,
      userId,
      compId,
      next
    );
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
