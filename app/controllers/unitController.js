const express = require('express');
const router = express.Router();
const UnitService = require('../services/unitService');

router.post('/', async (req, res, next) => {
  try {
    const { compId } = req.body;
    const result = await UnitService.ListByComp(compId, next);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
});

module.exports = router;
